/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.hashproject;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Hash {
    private static List<String> lookup = new LinkedList<String>(Arrays.asList(new String[101]));

    public static int convertKey(String key) {
        int sum_ascii = 0;
        for(int i=0 ; i<key.length(); i++){
            sum_ascii += key.codePointAt(i);
        }
        return sum_ascii;
    }   
    
    private static int hash(int key){
        return key%101;
    }
    
    public static void put(int key, String value){
        int index = hash(key);
        lookup.add(index, value);
    }
    
    public static String get(int key){
        int index = hash(key);
        return lookup.get(index);
    }
    
    public static String remove(int key){
        int index = hash(key);
        return lookup.remove(index);
    }
}
