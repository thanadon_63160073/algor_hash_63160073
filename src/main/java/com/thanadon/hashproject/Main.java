/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.hashproject;

import static com.thanadon.hashproject.Hash.*;
import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Main {
     public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        while(true){
            showMenu();
            String menu = kb.next();
            
            if(process(menu, kb))break;
            
            System.out.println("===================================");
        }
    }    

    private static boolean process(String menu, Scanner kb) {
        if (menu.equals("put")) {
            System.out.println("Enter Value(String): ");
            String value = kb.next();
            System.out.println("Enter Key(Integer): ");
            int key = kb.nextInt();
            put(key, value);
            System.out.println("\nInput Value Complete!!!");
        } else if (menu.equals("get")) {
            System.out.println("Enter Key(Integer): ");
            int key = kb.nextInt();
            System.out.println("\nValue of Key "+key+": "+get(key));
        } else if (menu.equals("remove")) {
            System.out.println("Enter Key(Integer): ");
            int key = kb.nextInt();
            remove(key);
            System.out.println("\nRemove Value Complete!!!");
        } else if (menu.equals("exit")) {
            return true;
        }
        return false;
    }

    private static void showMenu() {
        System.out.println("\n1.Put Value(put)");
        System.out.println("2.Get Value(get)");
        System.out.println("3.Remove Value(remove)");
        System.out.println("4.Exit Program(exit)");
        System.out.println("Enter Menu: ");
    }
}
